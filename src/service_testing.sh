#!/bin/bash

function use-db() {
     if [[ $1 == 'true' ]];
     then
        echo -e "\e[36mUsing database for testing service \e[33m${DB_TEST_CONTAINER_NAME}\e[39m";
        clear-database;
        migrate-up;
     else
         echo -e "\e[32mNot using database for testing service \e[33m${DB_TEST_CONTAINER_NAME}\e[39m";
     fi
}

function db-seed () {
     eval dbSeedCommand="$3";

     if [[ $dbSeedCommand != '' ]];
         then
             echo -e "\e[36mDatabase seed in service \e[33m${1}\e[36m start..\e[39m";
             docker-compose exec -u ${2} ${1} \
                ${dbSeedCommand} 2> /dev/null;

             code=$?;
             text="Database seed in service";
             report-push "\$text" ${1} ${code};
         fi
}

function service-testing() {
     echo -e "\e[36mTesting service \e[33m${1}\e[36m start..\e[39m";
     eval command="$3";
     docker-compose exec -u ${2} ${1} \
        ${command} 2> /dev/null;

     code=$?;
     text="Testing service";
     report-push "\$text" ${1} ${code};
}
