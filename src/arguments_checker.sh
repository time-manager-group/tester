#!/bin/bash

function arguments-check() {

for arg in "$@"
    do
  case ${arg} in
         'migrations-testing' )
                 migrationsTesting=true;
             ;;
         '--not-migrations-testing' )
                 notMigrationsTesting=true;
             ;;
         'services='* )
                  servicesString=$(sed "s/services=//" <<< $arg);
                  IFS=',' read -r -a serviceList <<< "$servicesString"
             ;;
         'services-ignore='* )
                 servicesIgnoreString=$(sed "s/services-ignore=//" <<< $arg);
                 IFS=',' read -r -a serviceIgnoreList <<< "$servicesIgnoreString"
             ;;
         'save' )
                #continue..
             ;;
         * )
                 echo -e "\e[31mArguments error: unknown argument \`${arg}\`!\e[0m";
                 exit 1;
             ;;
     esac
    done

    #Validation arguments
    if [[ ( $migrationsTesting == true && $notMigrationsTesting == true ) ]];
    then
        echo -e "\e[31mArguments error: no use together \`migrations-testing\` and \`--not-migrations-testing\`!\e[0m";
        exit 1;
    elif [[ ( (-n "$serviceList") && (-n "$serviceIgnoreList") )]];
    then
        echo -e "\e[31mArguments error: no use together \`services\` and \`services-ignore\`!\e[0m";
        exit 1;
    elif [[ ( ( (-n "$serviceList") || (-n "$serviceIgnoreList") ) && $notMigrationsTesting == true ) ]];
        then
            echo -e "\e[31mArguments error: no use together \`--not-migrations-testing\` and \`services\` or \`services-ignore\`!\e[0m";
            exit 1;
    fi
}

function in-array() {
    eval var="$1";
    eval array="$2";
    is=0;
    for item in "${array[@]}"; do
        [[ $var == "$item" ]] && is=1;
    done

    echo $is;
}
