#!/bin/bash

function migrate-test() {
                         echo -e "\e[36mMigrations test:migrate in service \e[33m${MIGRATOR_CONTAINER_NAME}\e[36m start..\e[39m";

                         docker-compose exec -u ${MIGRATOR_CONTAINER_USER} ${MIGRATOR_CONTAINER_NAME} \
                         ${MIGRATOR_CONTAINER_COMMAND_MIGRATE} 2> /dev/null;

                         code=$?;
                         text="Migrations test:migrate in service";
                         report-push "\$text" ${MIGRATOR_CONTAINER_NAME} ${code};

                         echo -e "\e[36mMigrations test:rollback in service \e[33m${MIGRATOR_CONTAINER_NAME}\e[36m start..\e[39m";

                         docker-compose exec -u ${MIGRATOR_CONTAINER_USER} ${MIGRATOR_CONTAINER_NAME} \
                         ${MIGRATOR_CONTAINER_COMMAND_ROLLBACK} 2> /dev/null;

                         code=$?;
                         text="Migrations test:rollback in service";
                         report-push "\$text" ${MIGRATOR_CONTAINER_NAME} ${code};
                     }

function migrate-up () {
                         echo -e "\e[36mMigrations up in service \e[33m${MIGRATOR_CONTAINER_NAME}\e[36m start..\e[39m";

                         docker-compose exec -u ${MIGRATOR_CONTAINER_USER} ${MIGRATOR_CONTAINER_NAME} \
                         ${MIGRATOR_CONTAINER_COMMAND_MIGRATE} 2> /dev/null;

                         code=$?;
                         text="Migrations up in service";
                         report-push "\$text" ${MIGRATOR_CONTAINER_NAME} ${code};
                    }

function migrate-down () {
                         echo -e "\e[36mMigrations down in service \e[33m${MIGRATOR_CONTAINER_NAME}\e[36m start..\e[39m";

                         docker-compose exec -u ${MIGRATOR_CONTAINER_USER} ${MIGRATOR_CONTAINER_NAME} \
                         ${MIGRATOR_CONTAINER_COMMAND_ROLLBACK} 2> /dev/null;

                         code=$?;
                         text="Migrations down in service";
                         report-push "\$text" ${MIGRATOR_CONTAINER_NAME} ${code};
                     }
