function clear-database () {
    echo -e "\e[36mClear database \e[33m${DB_TEST_CONTAINER_NAME}\e[36m start..\e[39m";

    docker-compose exec ${DB_TEST_CONTAINER_NAME} \
    mysql -h${DB_TEST_HOST} -u${DB_TEST_USERNAME} -p${DB_TEST_PASSWORD} -e \
    "DROP DATABASE IF EXISTS ${DB_TEST_DATABASE};CREATE DATABASE IF NOT EXISTS ${DB_TEST_DATABASE};" 2> /dev/null;

    code=$?;
    text="Clear database";
    report-push "\$text" ${DB_TEST_CONTAINER_NAME} ${code};
}
