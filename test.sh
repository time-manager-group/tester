#!/bin/bash

reportSave=false;

for arg in "$@"
    do
        if [ $arg == 'save' ]
        then
            reportSave=true;
        elif [ $arg == '-h' ]
        then
            #Documentation
           echo -e "\e[1;33;40m-------------------------------------------------------------------------------------------------------------------------------------------------\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[1;32m     Hello, developer!                                                                                                                         \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[1;32m     This platform for running integration and unit tests with use test database.                                                              \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[1;32m                                                                                                                                               \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[37m     There are the following options here:                                                                                                     \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[32m     1. \e[1;33m[without options]\e[0m                                \e[32m- \e[36mtests migrations with seeds and all services\e[32m;                                       \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[32m     3. \e[1;33msave\e[0m                                             \e[32m- \e[36msaves the test result to the report file\e[32m;                                           \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[32m     2. \e[1;33mmigrations-testing\e[0m                               \e[32m- \e[36mtests only migrations with seeds\e[32m;                                                   \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[32m     3. \e[1;33m--not-migrations-testing\e[0m                         \e[32m- \e[36mtests all services without testing migrations with seeds\e[32m;                           \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[32m     4. \e[1;33mservices=[service name 1,service name 2]\e[0m         \e[32m- \e[36mtests only certain services that can be enumerated through ','\e[32m;                     \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[32m     5. \e[1;33mservices-ignore=[service name 1,service name 2]\e[0m  \e[32m- \e[36mexcludes from testing only certain services that can be enumerated through ','\e[32m;     \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[32m     6. \e[1;33m-h\e[0m                                               \e[32m- \e[36mdocumentation for this platform\e[32m.                                                    \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[32m                                                                                                                                               \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m|\e[0m\e[1;32m     Happy testing, developer!                                                                                                                 \e[1;33;40m|\e[0m";
           echo -e "\e[1;33;40m-------------------------------------------------------------------------------------------------------------------------------------------------\e[0m";

           exit 0;
        fi
    done

if [ $reportSave == true ]
    then
        bash tester_save_report.sh $@;
    else
        bash tester.sh $@;
    fi
