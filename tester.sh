#!/bin/bash


# Check command arguments
source ./src/arguments_checker.sh

migrationsTesting=false;
notMigrationsTesting=false;
serviceList=();
serviceIgnoreList=();

arguments-check $@;

# Initialisations

source ./.env
source ./src/report.sh
source ./src/db_test.sh
source ./src/migrator.sh
source ./src/service_testing.sh



services=();

declare -A servicesConfig;

function readServiceConfig() {
     configValue=$(cat $1 | grep -E "^$2.+?" | sed "s/^$2=//");
     echo ${configValue};
}

absolutePathServicesDir=$(pwd);
absolutePathServicesDir="${absolutePathServicesDir}/${SERVICES_DIR}";
# Read services
for servicePath in ${SERVICES_DIR}*
    do
        serviceName=$(sed "s/.\/services\///" <<< $servicePath);
        services[${#services[@]}]=${serviceName};
        servicesConfig["${serviceName}|CONTAINER_NAME"]=$(readServiceConfig ${servicePath} 'CONTAINER_NAME');
        servicesConfig["${serviceName}|USER"]=$(readServiceConfig ${servicePath} 'USER');
        servicesConfig["${serviceName}|DB_SEED_COMMAND"]=$(readServiceConfig ${servicePath} 'DB_SEED_COMMAND');
        servicesConfig["${serviceName}|COMMAND"]=$(readServiceConfig ${servicePath} 'COMMAND');
        servicesConfig["${serviceName}|USE_DB"]=$(readServiceConfig ${servicePath} 'USE_DB');
    done

#Testing
cd ${APP_PATH}

testingReport=();
summaryStatus=0;

clear-database;

#Migrations testing
if [[ (-z "$serviceList") && (-z "$serviceIgnoreList") ]];
   then
       if [[ $migrationsTesting == true && $notMigrationsTesting == false ]];
          then
               migrate-test;
               clear-database;
               report-print;
       elif [[ $migrationsTesting == false && $notMigrationsTesting == false ]];
           then
               migrate-test;
       fi
else
    if [ $migrationsTesting == true ];
              then
                   migrate-test;
    fi
fi

#Testing services

if [ -n "$serviceList" ];
    then
        services=();
        services=${serviceList};
fi

for service in "${services[@]}"
    do
         isInServiceIgnoreList=$(in-array "\${service}" "\${serviceIgnoreList}");
               if [ $isInServiceIgnoreList -eq 0 ];
                    then
                        if [ -f "${absolutePathServicesDir}${service}" ];
                            then
                               containerName=${servicesConfig["${service}|CONTAINER_NAME"]};
                               user=${servicesConfig["${service}|USER"]};
                               dbSeedCommand=${servicesConfig["${service}|DB_SEED_COMMAND"]};
                               command=${servicesConfig["${service}|COMMAND"]};
                               useDb=${servicesConfig["${service}|USE_DB"]};

                               #Testing service
                               use-db ${useDb};
                               db-seed ${containerName} ${user} "\${dbSeedCommand}" ${useDb};
                               service-testing ${containerName} ${user} "\${command}" ${useDb};
                          else
                            text='Not found file';
                            report-push "\$text" "${absolutePathServicesDir}${service}" 1;
                          fi
                fi
    done

clear-database;

#Print report

report-print;
