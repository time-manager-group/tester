#!/bin/bash
source ./.env

mkdir -p ${TESTING_REPORTS_DIR};
reportFileName=${TESTING_REPORTS_DIR}testing-report-$(date +"%Y-%m-%d_%H:%m:%S").log

echo -e "\e[5;1;33;40m-------------------------------------\e[0m";
echo -e "\e[5;1;33;40m|\e[0m\e[1;32mTesting service and saving report..\e[5;1;33;40m|\e[0m";
echo -e "\e[5;1;33;40m-------------------------------------\e[0m";

bash tester.sh $@ &> ${reportFileName};

cat ${reportFileName};

